<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // Include database and object files
    include_once '../config/database.php';
    include_once './object.php';

    // Instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    // initialize object
    $product = new Product($db);
    
    // Query products
    $result = $product->read();
    $num = $result->num_rows;
    // Check if more than 0 record found
    if($num > 0) {
        // Products array
        $products_arr = array();
        $products_arr["results"] = array();

        // Retrieve table contents
        while ($row = mysqli_fetch_array($result)) {
            $product_item=array(
            "id" => $row["id"],
            "sku" => $row["sku"],
            "name" => $row["name"],
            "price" => $row["price"],
            "height" => $row["height"],
            "width" => $row["width"],
            "length" => $row["length"],
            "size" => $row["size"],
            "weight" => $row["weight"],
            "type" => $row["type"]
            );
            array_push($products_arr["results"], $product_item);
        }
        // Set response code - 200 OK
        http_response_code(200);
        // Show products data in json format
        echo json_encode($products_arr);
    }
    else {
        // Set response code - 404 Not found
        http_response_code(404);
        // No products found
        echo json_encode(
            array("message" => "No products found.")
        );
    }
?>