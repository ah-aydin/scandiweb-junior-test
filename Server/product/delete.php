<?php

    include_once '../common/cors.php';

    // Include database and object files
    include_once '../config/database.php';
    include_once './object.php';

    // Instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    $product = new Product($db);

    // Get posted data
    $data = json_decode(file_get_contents("php://input"));
    // Check that we have data
    if ($data->ids) {
        if ($product->remove($data->ids) == false) {
            http_response_code(400);
            echo json_encode(array("message" => "Products failed to be deleted.")); 
        }
        else {
            http_response_code(200);
            echo json_encode(array("message" => "Products were deleted."));
        }
    }
?>