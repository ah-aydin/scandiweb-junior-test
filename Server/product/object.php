<?php
    class Product {
        // Database connection
        private $conn;
        // Object properties
        public $id;
        public $sku;
        public $name;
        public $price;
        public $height;
        public $width;
        public $length;
        public $size;
        public $weight;
        public $type;

        // Maybe this might be needed in the future to specify the type of the product.
        // Current task does not require this though.
        public $category;

        // Constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function read() {
            // Select all query
            $query = "(SELECT p.id, p.sku AS sku, p.name, p.price, b.weight, NULL as size, NULL as height, NULL AS width, NULL AS length, 'book' AS type FROM product p INNER JOIN book b ON p.sku = b.sku) 
            UNION
            (SELECT p.id, p.sku AS sku, p.name, p.price, NULL as weight, d.size,  NULL as height, NULL AS width, NULL AS length, 'dvd' AS type FROM Product p INNER JOIN DVD d ON p.sku = d.sku)
            UNION
            (SELECT p.id, p.sku AS sku, p.name, p.price, NULL as weight, NULL as size, f.height, f.width, f.length, 'furniture' AS type FROM Product p INNER JOIN Furniture f ON p.sku = f.sku) ORDER BY id;
            ";
            
            // Get results
            $result = mysqli_query($this->conn, $query);
            return $result;
        }

        public function create() {
            // First check the type of the product
            // Then create the product and type entries in the tables
            $product_query = "INSERT INTO Product (sku, name, price) VALUES ('$this->sku', '$this->name', '$this->price');";
            if ($this->type == "book") {
                $book_query = "INSERT INTO Book (sku, weight) VALUES ('$this->sku', '$this->weight');";
                if (mysqli_query($this->conn, $product_query) == false ) {
                    return false;
                }
                if (mysqli_query($this->conn, $book_query) == false ){
                    // Remove product from table since this query failed
                    mysqli_query($this->conn, "DELETE FROM Product WHERE sku = '$this->sku'");
                    return false;
                }
                return true;
            }
            else if ($this->type == "dvd") {
                $dvd_query = "INSERT INTO DVD (sku, size) VALUES ('$this->sku', '$this->size');";
                if (mysqli_query($this->conn, $product_query) == false ) {
                    return false;
                }
                if (mysqli_query($this->conn, $dvd_query) == false ){
                    // Remove product from table since this query failed
                    mysqli_query($this->conn, "DELETE FROM Product WHERE sku = '$this->sku'");
                    return false;
                }
                return true;
            }
            else if ($this->type == "furniture") {
                $furniture_query = "INSERT INTO Furniture (sku, height, width, length) 
                                    VALUES ('$this->sku', '$this->height', '$this->width', '$this->length');";
                if (mysqli_query($this->conn, $product_query) == false ) {
                    return false;
                }
                if (mysqli_query($this->conn, $furniture_query) == false ){
                    // Remove product from table since this query failed
                    mysqli_query($this->conn, "DELETE FROM Product WHERE sku = '$this->sku'");
                    return false;
                }
                return true;
            }
            return false;
        }

        public function remove($ids) {
            $query = "DELETE FROM Product WHERE id IN (";
            foreach ($ids as $val) {
                $query .= "$val,";
            }
            $query .= "0);";
            if (mysqli_query($this->conn, $query) == false) {
                return false;
            }
            return true;
        }
    }
?>