<?php

    include_once '../common/cors.php';

    // Include database and object files
    include_once '../config/database.php';
    include_once './object.php';

    // Instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    $product = new Product($db);

    // Get posted data
    $data = json_decode(file_get_contents("php://input"));
    // Check that we have data
    if(!empty($data->sku) && !empty($data->name)  && !empty($data->price)  && !empty($data->type)) {
        // Set product property values
        $product->sku = $data->sku;
        $product->name = $data->name;
        $product->price = $data->price;
        $product->height = $data->height;
        $product->width = $data->width;
        $product->length = $data->length;
        $product->size = $data->size;
        $product->weight = $data->weight;
        $product->type = $data->type;

        // Create the product
        if($product->create()) {
            http_response_code(201);
            echo json_encode(array("message" => "Product was created."));
        }
        else {
            http_response_code(503);
            echo json_encode(array("message" => "Unable to create product."));    
        }
    }
    else {
        http_response_code(400);
        echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
    }
?>