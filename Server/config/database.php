<?php 

class Database {

    private $url;
    private $host;
    private $username;
    private $password;
    private $db_name;

    public $conn;

    public function getConnection() {
        $this->url = parse_url("mysql://b638e1eff102b9:0ff616c5@eu-cdbr-west-02.cleardb.net/heroku_b1ac9a5f433da53?reconnect=true");
        $this->host = $this->url["host"];
        $this->username = $this->url["user"];
        $this->password = $this->url["pass"];
        $this->db_name = substr($this->url["path"], 1);

        $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
        if (!$this->conn) {
            die("Failed to connect to database");
        }
        return $this->conn;
    }
}

?>