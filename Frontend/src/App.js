import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ProductList from './product/ProductList';
import AddProduct from './product/AddProduct';

import './App.css';

function App() {
  return (
    <div className='App'>
      <div className='row justify-content-center py-4'>
        <Router>
          <Switch>
            <Route exact path='/'             component={ProductList} />
            <Route exact path='/add-product'  component={AddProduct} />
          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
