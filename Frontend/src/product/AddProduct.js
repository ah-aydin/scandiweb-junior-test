import * as React from 'react';
import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { addProduct } from './redux/actions';

import './AddProduct.css'

const AddProduct = ({ addProduct }) => {

  const history = useHistory();

  const defaultFormData = {
    sku: '',
    name: '',
    price: 0,
    type: 'none',
    size: 0,
    width: 0,
    height: 0,
    length: 0,
    weight: 0
  };
  const [type, setType] = useState('none');
  const [formData, setFormData] = useState(defaultFormData);
  const [errors, setErrors] = useState(null);

  const onTypeChange = (e) => {
    e.preventDefault();
    setType(e.target.value);
  }

  const onChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});

  const onSubmit = (e) => {
    e.preventDefault();
    if (type === 'none') {
      setErrors(['Please select the type of the product.']);
      return;
    }
    setErrors(null);

    // Add product and return to listing
    addProduct(
      formData.sku, formData.name, formData.price, formData.height, 
      formData.width, formData.length, formData.size, formData.weight, type
    );
    history.push('/');
  }

  const none = () => {
    return (
      <div className='mb-3'>
        Please specify the type of the product.
      </div>
    );
  }

  const dvd = () => {
    return (
      <div className='mb-3 card px-3 pt-3' id='DVD'>
        <div className='mb-3'>
          <label htmlFor='size' className='form-label'>Size (MB)</label>
          <input type='number' min={0} className='form-control' id='size' name='size' onChange={(e) => onChange(e)} defaultValue='0' required/>
          <div id='sizeHelp' className='form-text'>Please, provide size</div>
        </div>
        <div className=''>
          <p id='dvdDescription' className='fs-6 addForm__description'>Please provide size of the DVD in MBs.</p>
        </div>
      </div>
    );
  }

  const furniture = () => {
    return (
      <div className='mb-3 card px-3 pt-3' id='Furniture'>
        <div className='mb-3'>
          <label htmlFor='height' className='form-label'>Height (CM)</label>
          <input type='number' min={0} className='form-control' id='height' name='height' onChange={(e) => onChange(e)} defaultValue='0' required/>
          <div id='heightHelp' className='form-text'>Please, provide height</div>
        </div>
        <div className='mb-3'>
          <label htmlFor='width' className='form-label'>Width (CM)</label>
          <input type='number' min={0} className='form-control' id='width' name='width' onChange={(e) => onChange(e)} defaultValue='0' required/>
          <div id='widthHelp' className='form-text'>Please, provide width</div>
        </div>
        <div className='mb-3'>
          <label htmlFor='length' className='form-label'>Length (CM)</label>
          <input type='number' min={0} className='form-control' id='length' name='length' onChange={(e) => onChange(e)} defaultValue='0' required/>
          <div id='lengthHelp' className='form-text'>Please, provide length</div>
        </div>
        <div className=''>
          <p id='furnitureDescription' className='fs-6 addForm__description'>Please provide dimensions in HxWxL format</p>
        </div>
      </div>
    );
  }

  const book = () => {
    return (
      <div className='mb-3 card px-3 pt-3' id='Book'>
        <div className='mb-3'>
          <label htmlFor='weight' className='form-label'>Weight (KG)</label>
          <input type='number' min={0} className='form-control' id='weight' step='0.01' name='weight' onChange={(e) => onChange(e)} defaultValue='0' required/>
          <div id='weightHelp' className='form-text'>Please, provide weight</div>
        </div>
        <div className=''>
          <p id='bookDescription' className='fs-6 addForm__description'>Please provide the weight in KG</p>
        </div>
      </div>
    );
  }

  /**
   * Return the apropiate portion htmlFor the chosen product type 
   */
  const typeForm = (id) => {
    switch(id) {
      case 'none':      return none();
      case 'dvd':       return dvd();
      case 'furniture': return furniture();
      case 'book':      return book();
      default:          return <div/>
    }
  }

  return (
    <div className='addProduct col col-md-8'>
      <div className='addProduct__header mb-3'>
        <h2>Product Add</h2>
      </div>

      <div className='addProduct__form mb-3'>
        <form id='product_form' onSubmit={(e) => onSubmit(e)}>

          <div className='mb-3'>
            <label htmlFor='sku' className='form-label'>SKU</label>
            <input type='text' className='form-control' id='sku' name='sku' onChange={(e) => onChange(e)} defaultValue='' required/>
          </div>
          <div className='mb-3'>
            <label htmlFor='name' className='form-label'>Name</label>
            <input type='text' className='form-control' id='name' name='name' onChange={(e) => onChange(e)} defaultValue='' required/>
          </div>
          <div className='mb-3'>
            <label htmlFor='price' className='form-label'>Price ($)</label>
            <input type='number' min={0} className='form-control' id='price' name='price' onChange={(e) => onChange(e)} defaultValue='0' required/>
          </div>

          <div>
            <label htmlFor='productType' className='form-label'>Type Switcher</label>
            <select 
              className='form-select form-select-lg mb-3' aria-label='.form-select-lg example' 
              id='productType' name='type' onChange={(e) => onTypeChange(e)} required
              >
              <option selected value='none'>Please select the product type</option>
              <option value='dvd'>DVD</option>
              <option value='furniture'>Furniture</option>
              <option value='book'>Book</option>
            </select>
          </div>

          { typeForm(type) }

          <div className='addProduct__formFotter'>
            <input type='submit' className='btn btn-primary addProduct__formButton' value='Save' />
            <Link className='btn btn-outline-danger addProduct__formButton' to='/' onClick={(e) => setFormData(defaultFormData)}>Cancel</Link>
          </div>
        </form>
      </div>

      <div className='addProduct__footer'>
        <div className='addProduct__errors'>
          { errors ? errors.map((error, index) => (
            <div className='card text-white bg-danger mb-1'>
              <div className='card-header'>{error}</div>
            </div>
          )) : <div />}
        </div>
      </div>
    </div>
  );
}

export default connect(null, { addProduct })(AddProduct);