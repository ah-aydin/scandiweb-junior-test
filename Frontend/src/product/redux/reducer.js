

const initialState = {
    productList: [
        
    ]
};

export const FETCH_PRODUCTS = 'product/fetchProducts';
export const FETCH_PRODUCTS_FAIL = 'product/fetchProductsFail';
export const ADD_PRODUCT = 'product/add';
export const ADD_PRODUCT_FAIL = 'product/addFail';
export const REMOVE_PRODUCTS = 'product/remove';
export const REMOVE_PRODUCTS_FAIL = 'product/removeFail';

export default function (state=initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                productList: payload.results
            };
        case FETCH_PRODUCTS_FAIL:
            return {
                ...state,
                productList: []
            };
        case ADD_PRODUCT:
        case ADD_PRODUCT_FAIL:
        case REMOVE_PRODUCTS:
        case REMOVE_PRODUCTS_FAIL:
        default:
            return state;
    }
}