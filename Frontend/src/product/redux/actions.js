import axios from 'axios';
import {
    FETCH_PRODUCTS,
    FETCH_PRODUCTS_FAIL,
    ADD_PRODUCT,
    ADD_PRODUCT_FAIL,
    REMOVE_PRODUCTS,
    REMOVE_PRODUCTS_FAIL
} from './reducer';

const url = 'https://scandiweb-junior-web-task.herokuapp.com/';
// const url = 'http://127.0.0.7/';
//http://127.0.0.7/product/read_page.php

export const fetchProducts = () => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${url}product/read_page.php`, config);
        dispatch({
            type: FETCH_PRODUCTS,
            payload: res.data
        });
    } catch (e) {
        dispatch({
            type: FETCH_PRODUCTS_FAIL,
            payload: e.response
        });
    }
}

export const addProduct = (sku, name, price, height, width, length, size, weight, type) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = JSON.stringify({
        sku, name, price, height, width, length, size, weight, type
    });

    try {
        const res = await axios.post(`${url}product/create.php`, body, config);
        dispatch({
            type: ADD_PRODUCT,
            payload: res.data
        });
        dispatch(fetchProducts());
    } catch (e) {
        dispatch({
            type: ADD_PRODUCT_FAIL,
            payload: e.response
        });
    }
}

export const removeProducts = (ids) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = {
        ids: ids
    };
    console.log(body);

    try {
        await axios.post(`${url}product/delete.php`, body, config);
        dispatch({
            type: REMOVE_PRODUCTS
        });
        dispatch(fetchProducts());
    } catch (e) {
        dispatch({
            type: REMOVE_PRODUCTS_FAIL
        });
    }
}