import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchProducts, removeProducts } from './redux/actions';

import './ProductList.css';

const ProductList = ({ fetchProducts, removeProducts, prods}) => {
    // Load products when entering the page
    useEffect(() => {
        fetchProducts();
    }, []);

    // List of id's of products that are going to be removed
    var removals = [];

    const addToRemoval = (id) => {
        removals.push(id);
    }

    const removeFromRemoval = (id) => {
        const index = removals.indexOf(id);
        if (index > -1) {
            removals.splice(index, 1);
        }
    }

    const massDelete = (e) => {
        removeProducts(removals);
        // Reset the checkboxes
        removals = [];
        var elems = document.querySelectorAll('.delete-checkbox');
        for (let i in elems) {
            elems[i].checked = false;
        }
    }

    // For checkboxes
    const onClick = (e) => {
        if (e.target.checked == true) {
            addToRemoval(parseInt(e.target.value));
        } else if (e.target.checked == false) {
            removeFromRemoval(parseInt(e.target.value));
        }
    }

    const productSpecificData = (product) => {
        if (product.type === 'book') {
            return `Weight: ${product.weight}KG`;
        } else if (product.type === 'dvd') {
            return `Size: ${product.size}`;
        } else if (product.type === 'furniture') {
            return `Dimension: ${product.height}x${product.width}x${product.length}`;
        } else {
            return '';
        }
    }

    return (
        <div className='productList col col-md-12 px-4'>
            <div className='productList__header'>
                <h2>Product List</h2>
                <div className='productList__buttonGroup'>
                    <Link className='btn btn-primary productList__button' to='/add-product'>ADD</Link>
                    <div className='btn btn-outline-danger productList__button' id='delete-product-btn' onClick={ (e) => massDelete(e) }>MASS DELETE</div>
                </div>
            </div>
            <hr/>
            <div className='productList__productGrid'>
                { prods ? prods.map((product, index) => (
                    <div className='card bg-light productList__product px-2 pt-3 text-center'>
                        <input type='checkbox' value={product.id} className='delete-checkbox' onClick={(e) => onClick(e)}/>
                        <div className='card-body'>
                            <p>{product.sku}</p>
                            <p>{product.name}</p>
                            <p>{(product.price)}$</p>
                            <p>{productSpecificData(product)}</p>
                        </div>
                    </div>
                )) : <div/>}
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    prods: state.product.productList
});

export default connect(mapStateToProps, { fetchProducts, removeProducts })(ProductList);