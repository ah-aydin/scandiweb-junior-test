import { combineReducers } from 'redux';

import product from './product/redux/reducer';

export default combineReducers({
    product
});