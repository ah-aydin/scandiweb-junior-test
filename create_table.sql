CREATE TABLE Product (
  id int NOT NULL AUTO_INCREMENT,
  sku varchar(255) NOT NULL UNIQUE,
  name varchar(255) NOT NULL UNIQUE,
  price int(11),
  PRIMARY KEY (id)
);

CREATE TABLE DVD (
  sku varchar(255) NOT NULL,
  size int,
  FOREIGN KEY (sku) REFERENCES Product(sku) ON DELETE CASCADE
);

CREATE TABLE Furniture (
  sku varchar(255) NOT NULL,
  height int ,
  width int,
  length int,
  FOREIGN KEY (sku) REFERENCES Product(sku) ON DELETE CASCADE
);

CREATE TABLE Book (
  sku varchar(255) NOT NULL,
  weight float,
  FOREIGN KEY (sku) REFERENCES Product(sku) ON DELETE CASCADE
);