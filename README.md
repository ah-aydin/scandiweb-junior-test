## React environment setup

    cd Frontend
    npm install react react-redux redux axios redux-thunk redux-devtools-extension

## Running the PHP server

Compile the react build

    npm run build

Move the contents of the ``./Frontend/build`` folder to ``./Server``.


## Live demo

Visit ``https://scandiweb-junior-web-task.herokuapp.com``